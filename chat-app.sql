-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th7 20, 2022 lúc 02:41 PM
-- Phiên bản máy phục vụ: 5.7.36
-- Phiên bản PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `chat-app`
--
CREATE DATABASE IF NOT EXISTS `chat-app` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `chat-app`;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chat_contents`
--

DROP TABLE IF EXISTS `chat_contents`;
CREATE TABLE IF NOT EXISTS `chat_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `friends`
--

DROP TABLE IF EXISTS `friends`;
CREATE TABLE IF NOT EXISTS `friends` (
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `user_id_1_2` (`user_id`,`friend_id`),
  UNIQUE KEY `user_id_1_3` (`friend_id`,`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `friends`
--

INSERT INTO `friends` (`user_id`, `friend_id`, `status`) VALUES
(1, 3, 0),
(1, 4, 0),
(1, 5, 0),
(2, 3, 0),
(2, 4, 0),
(2, 5, 0),
(3, 4, 0),
(3, 5, 0),
(4, 5, 0),
(64, 2, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `histories`
--

DROP TABLE IF EXISTS `histories`;
CREATE TABLE IF NOT EXISTS `histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key_search` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_search` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_3` (`user_id`,`key_search`),
  UNIQUE KEY `user_id_4` (`user_id`,`user_search`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `histories`
--

INSERT INTO `histories` (`id`, `user_id`, `create_date`, `key_search`, `user_search`) VALUES
(38, 1, '2022-07-01 09:53:26', 'chu', NULL),
(36, 1, '2022-07-01 03:05:52', NULL, 1),
(34, 1, '2022-07-01 02:53:56', 'chdj', NULL),
(35, 1, '2022-07-01 03:01:07', 'chuan', NULL),
(33, 1, '2022-07-01 02:53:56', 'chd', NULL),
(30, 1, '2022-07-01 02:48:10', 'c', NULL),
(31, 1, '2022-07-01 02:53:16', 'sw', NULL),
(32, 1, '2022-07-01 02:53:55', 'ch', NULL),
(37, 1, '2022-07-01 09:53:01', 'chua', NULL),
(39, 1, '2022-07-05 04:37:34', 'na', NULL),
(42, 64, '2022-07-19 21:08:18', 'c', NULL),
(43, 64, '2022-07-19 21:17:08', NULL, 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `rooms`
--

DROP TABLE IF EXISTS `rooms`;
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `image`, `last_update`) VALUES
(12, 'chuan', '591656659012531.png', '2022-07-20 20:30:56'),
(13, 'chuan', '451656659937236.png', '2022-07-20 20:30:56'),
(14, 'chuan, tien, Thai Anh', 'defaultRoom.png', '2022-07-20 20:30:56'),
(15, 'chuan, tien, Thai Anh', 'defaultRoom.png', '2022-07-20 20:30:56'),
(16, 'chuan, tien, Thai Anh', 'defaultRoom.png', '2022-07-20 20:30:56'),
(17, 'chuan, tien, Thai Anh', 'defaultRoom.png', '2022-07-20 20:30:56'),
(18, 'cac', '501658325442877.png', '2022-07-20 20:57:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `image`, `password`, `email`, `phone`, `status`, `create_date`, `update_date`) VALUES
(1, 'chuan', 'chuan.png', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'natswarchuan@gmail.com', '090871368971', 1, '2022-06-21 23:50:46', '2022-06-21 23:50:46'),
(2, 'tien', 'Tien.png', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'baobaote00@gmail.com', '0908987712', 1, '2022-06-21 23:50:46', '2022-06-21 23:50:46'),
(3, 'Thai Anh', 'ThaiAnh.png', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'zakudatashida@gmail.com', '0706336801', 1, '2022-06-21 23:50:46', '2022-06-21 23:50:46'),
(4, 'Uyen', 'Uyen.png', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'huynhduongmyuyen.tdc2019@gmail.com', '0585081903', 1, '2022-06-21 23:50:46', '2022-06-21 23:50:46'),
(64, 'chuan', 'default.png', '1fafab9869e1e8dfa8359f43b342ec157a42bb89', 'chuanvm.80.student@fit.tdc.edu.vn', '0908713697', 1, '2022-07-19 20:26:43', '2022-07-19 20:54:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users_rooms`
--

DROP TABLE IF EXISTS `users_rooms`;
CREATE TABLE IF NOT EXISTS `users_rooms` (
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`,`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users_rooms`
--

INSERT INTO `users_rooms` (`user_id`, `room_id`) VALUES
(1, 13),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(2, 13),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(3, 13),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(4, 13),
(4, 15),
(4, 16),
(4, 17),
(4, 18),
(5, 13),
(64, 15),
(64, 16),
(64, 17),
(64, 18);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
