require('dotenv').config({ path: __dirname + '/../.env' });
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
const IpAddress = require('./app/config/IpAddress/index');
const port = process.env.PORT;
const imageRouter = require('./app/routers/Image');
const userRouter = require('./app/routers/User');
const contentRouter = require('./app/routers/Content');
const friendRouter = require('./app/routers/Friend');
const roomRouter = require('./app/routers/Room');
const cookieParser = require('cookie-parser')

app.use(cookieParser());
app.use(bodyParser.json());
app.use(session({
    resave: true, 
    saveUninitialized: true, 
    secret: 'somesecret', 
    cookie: { maxAge: 365*24*60*60*1000 }}));
app.use(bodyParser.urlencoded({ extended: false }));

app.set('port', port);
const cors = require("cors");
app.use(cors());

app.get('/', function (req, res) {
    return res.send({  status: 200, message: 'api app chat' })
});

app.get('/api/', function (req, res) {
    res.status(200).send({});
});
app.use('/api/image', imageRouter);
app.use('/api/user', userRouter);
app.use('/api/content', contentRouter);
app.use('/api/room', roomRouter);
app.use('/api/friend', friendRouter);

app.listen(port, IpAddress.logPort);

module.exports = app;