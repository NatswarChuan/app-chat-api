const express = require('express');
const multer = require('multer');
const UserController = require('../controller/UserController');
const upload = multer({
    limits: {
        fileSize: Infinity,
    }
});

const router = express.Router();

router.get('/search', (req, res) => UserController.searchUser(req, res));

router.post('/login', (req, res) => UserController.login(req, res));

router.get('/profile', (req, res) => UserController.getProfile(req, res));

router.get('/logout', (req, res) => UserController.logout(req, res));

router.get('/rooms', (req, res) => UserController.getRooms(req, res));

router.get('/histories', (req, res) => UserController.getHistories(req, res));

router.post('/check-password', (req, res) => UserController.checkPassword(req, res));

router.get('/info/:id', (req, res) => UserController.getInfoById(req, res));

router.post('/register', upload.single('image'), (req, res) => UserController.register(req, res));

router.post('/change-info', upload.single('image'), (req, res) => UserController.changeInfo(req, res));

router.post('/otp/register', (req, res) => UserController.otpRegister(req, res));

router.post('/forgot', (req, res) => UserController.forgotPassword(req, res));

router.post('/otp/forgot-password', (req, res) => UserController.otpForgotPassword(req, res));

router.post('/change-password-forgot', (req, res) => UserController.changeForgotPassword(req, res));

router.post('/change-password', (req, res) => UserController.changePassword(req, res));

router.post('/history/search', (req, res) => UserController.historySearch(req, res));

module.exports = router;