require('dotenv').config({ path: __dirname + '../../.env' });
const express = require('express');
var dbConn = require('../config/Db/config');
const Image = require('../models/Image');
const User = require('../models/User');

const router = express.Router();
const per_page = 10;

router.get('/room/:id', function (req, res) {
    if (!User.checkUser(req, res)) {
        return res.status(403).send({});
    }
    if (!req.params.id) {
        return res.status(400).send({});
    }
    const id = req.params.id;
    const start = req.query && req.query.page ? per_page * (req.query.page - 1) : 0;
    const sql = 'SELECT `rooms`.*, `chat_contents`.`user_id` AS userId, `chat_contents`.`content` AS userContent, `users`.`name` AS userName, `users`.`image` AS userImage FROM `rooms` JOIN `chat_contents` ON `chat_contents`.`room_id` = `rooms`.`id` JOIN `users` ON `users`.`id` = `chat_contents`.`user_id` WHERE `rooms`.`id` = ? LIMIT ?,?'

    dbConn.query(sql, [id, start, per_page + 1], function (error, results) {
        if (error) {
            return res.status(400).send({});
        }
        else if (!results || !results.length) {
            return res.status(204).send({});
        }
        else {
            const isEnd = results.length <= per_page;
            if (!isEnd) {
                results.pop();
            }
            let data = {
                id: results[0].id,
                name: results[0].name,
                image: results[0].image,
                isEnd: isEnd,
                contents: []
            }
            results.forEach(item => {
                data.contents.push(
                    {
                        id: item.userId,
                        name: item.userName,
                        image: Image.genImageLink(item.userImage),
                        content: item.userContent
                    }
                )
            });
            return res.status(200).send(data);
        }
    });


});

module.exports = router;