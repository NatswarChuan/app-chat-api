const fs = require('fs');
const express = require('express');
const router = express.Router();

router.get('/get/:img_name', function (req, res) {
    const img_name = req.params.img_name;
    const path = process.env.IMG_DIR + img_name;
    if (fs.existsSync(path)) {
        return res.sendFile(path);
    }else{
        return res.send({"status": 400});
    }
});


module.exports = router;