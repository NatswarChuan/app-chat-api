const express = require('express');
require('dotenv').config({ path: __dirname + '../../.env' });
const multer = require('multer');
const RoomController = require('../controller/RoomController');
const upload = multer({
    limits: {
        fileSize: Infinity,
    }
});

const router = express.Router();

router.post('/create', upload.single('image'), (req,res)=> RoomController.create(req,res));

router.get('/users/:id',(req,res)=> RoomController.getUsersByRoomId(req,res));

router.post('/invite', (req,res)=> RoomController.inviteUserToRoomId(req,res));

router.post('/leaving', (req,res)=> RoomController.leavingRoom(req,res));

router.post('/update', upload.single('image'), (req,res)=> RoomController.updateRoom(req,res));
module.exports = router;