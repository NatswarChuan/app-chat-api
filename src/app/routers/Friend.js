require('dotenv').config({ path: __dirname + '../../.env' });
const express = require('express');
var dbConn = require('../config/Db/config');
const FriendController = require('../controller/FriendController');
const Image = require('../models/Image');
const User = require('../models/User');
const router = express.Router();

router.post('/add', (req, res) => FriendController.addFriend(req, res));

router.get('/get', (req, res) => FriendController.getFiends(req,res)); 

router.post('/accepted', (req, res) => FriendController.acceptedFiends(req,res)); 

router.post('/unfriend', (req, res) => FriendController.unFriend(req,res)); 

module.exports = router;
