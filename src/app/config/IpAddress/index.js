require('dotenv').config({ path: __dirname + '/../../../../../.env' });
var dbConn = require('./../Db/config');
const { networkInterfaces } = require('os');
const nets = networkInterfaces();

const port = process.env.PORT;
const baseURL = process.env.BASE_URL;

function logPort() {
    setTimeout(function querys() {
        dbConn.query('SELECT version()', function (error) {
            if (error) console.log('Error ' + error);
            logAddress();
            setTimeout(querys, 300000);
        });
    }, 0);
}

function logAddress() {
    const ipAddress = getIpAddress();
    ipAddress.forEach(element => {
        console.log(`http://${baseURL}:${port}`);
    });
}

function getIpAddress() {
    return [baseURL];
}

module.exports = { logPort, getIpAddress }