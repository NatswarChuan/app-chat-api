require('dotenv').config({ path: __dirname + '../../.env' });
var dbConn = require('../config/Db/config');
const User = require('../models/User');
const Image = require('../models/Image');
const multer = require('multer')
const upload = multer({
    limits: {
        fileSize: Infinity,
    }
});
const defaultRoomAvatar = 'defaultRoom.png';

const SQL = {
    SELECT_USER_NAME_BY_ID: 'SELECT `name` FROM `users` WHERE 1 ',
    CREATE_ROOM: 'INSERT INTO `rooms`(`name`, `image`) VALUES (?,?)',
    INSERT_USER_TO_ROOM: 'INSERT IGNORE INTO `users_rooms`(`user_id`, `room_id`) VALUES ',
    SELECT_USERS_BY_ROOM_ID: 'SELECT `users`.`id` AS id, `users`.`name` AS name, `users`.`image` AS image, `rooms`.`id` AS roomId, `rooms`.`name` AS roomName, `rooms`.`image` AS roomImage, `rooms`.`last_update` AS updateDate FROM `users_rooms` JOIN `users` ON `users`.`id` = `users_rooms`.`user_id` JOIN `rooms` ON `rooms`.`id` = `users_rooms`.`room_id` WHERE `room_id` = ?',
    LEAVING_ROOM: 'DELETE FROM `users_rooms` WHERE `user_id` = ? AND `room_id` = ?',
    UPDATE_ROOM: 'UPDATE `rooms` SET `name`= ?,`image`= ?,`last_update`= CURRENT_TIMESTAMP() WHERE `id`= ? AND `last_update` = ?',
}

module.exports = class Room {

    static query(req, res, sql, values, queryFunction) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            }
            else if (!results || !results.length) {
                return res.status(204).send({});
            }
            else {
                return queryFunction(req, res, results);
            }
        });
    }

    static insertOrUpdate(req, res, sql, values, queryFunction) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            } else if (!results) {
                return res.status(204).send({});
            }
            else {
                return queryFunction(req, res, results);
            }
        });
    }

    static create(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.userIds) {
            return res.status(400).send({});
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const userIdsList = [...req.body.userIds, id];
        let sql = SQL.SELECT_USER_NAME_BY_ID;
        userIdsList.forEach(userId => {
            sql += 'OR `id` = ? ';
        });
        return Room.query(req, res, sql, userIdsList, Room.queryCreateRoom);
    }

    static queryCreateRoom(req, res, results) {
        const roomName = req.body.roomName ? req.body.roomName : Room.genRoomName(results);
        const imagePath = process.env.IMG_DIR;
        const fileUpload = new Image(imagePath);
        let filename = new Promise((resolve) => { resolve() });
        let fileName = defaultRoomAvatar;

        if (req.file) {
            fileName = `${Math.floor(Math.random() * 99)}${Number(Date.now())}.png`;
            filename = fileUpload.save(req.file.buffer, fileName);
        }
        let roomId;

        const create_room = new Promise((resolve,reject) => {
            dbConn.query(SQL.CREATE_ROOM, [roomName, fileName], function (error, results) {
                if (error) {
                    reject();
                }
                roomId = results.insertId;
                resolve();
            })
        })

        Promise.all([create_room, filename]).then(function (results) {
            let sql_rooms_users = SQL.INSERT_USER_TO_ROOM;
            const id = req.session.user ? req.session.user.id : req.cookies.user.id;
            const userIdsList = [...req.body.userIds, id];
            let data = [];
            for (let index = 0; index < userIdsList.length; index++) {
                if (index == userIdsList.length - 1) {
                    sql_rooms_users += '(?,?)';
                } else {
                    sql_rooms_users += '(?,?),';
                }
                data.push(userIdsList[index]);
                data.push(roomId);
            }
            dbConn.query(sql_rooms_users, data, function (error, results) {
                if (error) {
                    return res.status(400).send({});
                }
                return res.status(200).send({ id: roomId });
            })
        }).catch(()=>{
            return res.status(400).send({});
        });
    }

    static genRoomName(results) {
        let roomName = '';
        for (let index = 0; index < 3; index++) {
            if (index < results.length && index != 2) {
                roomName += results[index].name + ', ';
            } else {
                roomName += results[index].name;
            }
        }
        return roomName;
    }

    static getUsersByRoomId(req, res) {
        if (!req.params.id) {
            return res.status(400).send({});
        }
        const id = req.params.id;
        const values = [id];
        return Room.query(req, res, SQL.SELECT_USERS_BY_ROOM_ID, values, Room.queryGetusersByRoomId);
    }

    static queryGetusersByRoomId(req, res, results) {
        const room = {
            id: results[0].roomId,
            name: results[0].roomName,
            updateDate: results[0].updateDate,
            image: Image.genImageLink(results[0].roomImage),
            users: Image.genUserImageLink(results)
        }
        room.users.map((user) => {
            delete user.roomId;
            delete user.roomName;
            delete user.roomImage;
            delete user.updateDate;
            return user;
        })
        return res.status(200).send(room);
    }

    static inviteUserToRoomId(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.userId || !req.body.roomId) {
            return res.status(400).send({});
        }
        const userId = req.body.userId;
        const roomId = req.body.roomId;
        const sql_insert = SQL.INSERT_USER_TO_ROOM + ' ( ?, ?)';
        const values = [userId, roomId];
        return Room.insertOrUpdate(req, res, sql_insert, values, Room.queryInviteUserToRoomId);
    }

    static queryInviteUserToRoomId(req, res, results) {
        const roomId = req.body.roomId;
        const values = [roomId];
        return Room.query(req, res, SQL.SELECT_USERS_BY_ROOM_ID, values, Room.queryGetusersByRoomId);
    }

    static leavingRoom(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }

        if (!req.body.roomId) {
            return res.status(400).send({});
        }

        const roomId = req.body.roomId;
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id, roomId];

        return Room.insertOrUpdate(req, res, SQL.LEAVING_ROOM, values, Room.queryLeavingRoom);
    }

    static queryLeavingRoom(req, res, results) {
        return User.getRooms(req, res);
    }

    static updateRoom(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }

        if (!req.body.roomId || !req.body.roomName || !req.body.updateDate || !req.body.oldImage) {
            return res.status(400).send({});
        }

        const roomId = req.body.roomId;
        const roomName = req.body.roomName;
        const updateDate = new Date(req.body.updateDate);
        let image = Image.genImageNameFromLink(req.body.oldImage);
        const imagePath = process.env.IMG_DIR;
        const fileUpload = new Image(imagePath);
        if (image != defaultRoomAvatar && req.file) {
            Image.remove(image);
        }
        if (req.file) {
            image = `${Math.floor(Math.random() * 99)}${Number(Date.now())}.png`;
            fileUpload.save(req.file.buffer, image);
        }
        const values = [roomName, image, roomId, updateDate];
        return Room.insertOrUpdate(req,res, SQL.UPDATE_ROOM, values, Room.queryUpdateRoom);
    }

    static queryUpdateRoom(req,res,results){
        const roomId = req.body.roomId;
        const values = [roomId];
        return Room.query(req, res, SQL.SELECT_USERS_BY_ROOM_ID, values, Room.queryGetusersByRoomId);
    }
}