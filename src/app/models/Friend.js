require('dotenv').config({ path: __dirname + '../../.env' });
var dbConn = require('../config/Db/config');
const User = require('../models/User');
const Image = require('../models/Image');
const multer = require('multer')
const upload = multer({
    limits: {
        fileSize: Infinity,
    }
});

const SQL = {
    SELECT_FRIEND: 'SELECT `users`.`id`, `users`.`name`,`users`.`image` FROM `friends` JOIN `users` ON `users`.`id` = `friends`.`friend_id` WHERE `friends`.`user_id` = ? AND `friends`.`status` = 1',
    ADD_FRIEND: 'INSERT INTO `friends`(`user_id`, `friend_id`) VALUES ( ?, ?)',
    ADD_FRIEND_ACCEPT: 'INSERT INTO `friends`(`user_id`, `friend_id`, `status`) VALUES ( ?, ?, 1)',
    ACCEPTED_FRIEND: 'UPDATE `friends` SET `status`= 1 WHERE `friend_id` = ? AND `user_id` = ?',
    UNDFRIEND: 'DELETE FROM `friends` WHERE (`user_id`= ? AND `friend_id`= ?) OR (`friend_id`= ? AND `user_id` = ?)'
}

module.exports = class Friend {

    static query(req, res, sql, values, queryFunction) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            }
            else if (!results || !results.length) {
                return res.status(204).send({});
            }
            else {
                return queryFunction(req, res, results);
            }
        });
    }

    static insertOrUpdate(req, res, sql, values, queryFunction) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            } else if (!results) {
                return res.status(204).send({});
            }
            else {
                return queryFunction(req, res, results);
            }
        });
    }

    static getFiends(req, res) {
        if (!User.checkUser(req, res)) {

            return res.status(403).send({});
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id];
        return Friend.query(req, res, SQL.SELECT_FRIEND, values, (req, res, results) => res.status(200).send(Image.genUserImageLink(results)))
    }

    static addFriend(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.userId) {
            return res.status(400).send({});
        }
        const values = Friend.getDataAdOrdAccept(req, res);
        return Friend.insertOrUpdate(req, res, SQL.ADD_FRIEND, values, (req, res, results) => res.status(200).send({}));
    }

    static acceptedFiends(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.userId) {
            return res.status(400).send({});
        }
        const values = Friend.getDataAdOrdAccept(req, res);
        return Friend.insertOrUpdate(req, res, SQL.ACCEPTED_FRIEND, values, Friend.queryAcceptedFiends);
    }

    static queryAcceptedFiends(req, res, results) {
        const values = Friend.getDataAdOrdAccept(req, res);
        return Friend.insertOrUpdate(req, res, SQL.ADD_FRIEND_ACCEPT, values, (req, res, results) => Friend.getFiends(req, res));
    }

    static getDataAdOrdAccept(req, res) {
        const userId = req.body.userId;
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        return [id, userId];
    }

    static unFriend(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.userId) {
            return res.status(400).send({});
        }

        const userId = req.body.userId;
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id, userId, id, userId];
        return Friend.insertOrUpdate(req, res, SQL.UNDFRIEND, values, Friend.queryUnFriend);
    }

    static queryUnFriend(req, res,results){
        return Friend.getFiends(req, res);
    }
}