const dbConn = require("../config/Db/config");
const Image = require('../models/Image');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
require('dotenv').config({ path: __dirname + '../../../.env' });

const per_page = 10;
const regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}/;
const regexEmail = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const regexPhone = /^(\+84|0[3|5|7|8|9])+([0-9]{8})\b/;
const regexOtp = /^([0-9]{6})/;

const serverEmail = process.env.MAIL_ADDRESS;
const serverPassword = process.env.MAIL_PASSWORD;

const defaultUserAvatar = 'default.png';

const SQL = {
    SEARCH_USER: 'SELECT `id`,`name`,`image` FROM `users` WHERE (`name` LIKE ? OR `email` LIKE ? OR `phone` LIKE ? ) AND `status` = 1 LIMIT ?,?',
    INSERT_KEY_SEARCH_HISTORY: 'INSERT IGNORE INTO `histories`( `user_id`,  `key_search`) VALUES (?,?)',
    REMOVE_HISTORY: 'DELETE FROM `histories` WHERE `id` IN( SELECT * FROM ( SELECT id FROM `histories` WHERE `user_id` = ? ORDER BY `create_date` DESC LIMIT 10,1) AS h )',
    SELECT_USER_LOGIN: 'SELECT `id`,`name`,`image`,`email`,`phone`,`update_date` as updateDate FROM `users` WHERE `email` = ? AND `password` = SHA1(?) AND `status` = 1',
    SELECT_PROFILE_BY_ID: 'SELECT `id`,`name`,`image`,`email`,`phone`,`update_date` as updateDate FROM `users` WHERE `id` = ? AND `status` = 1',
    SELECT_ROOM_BY_USER_ID: 'SELECT `users`.`id` AS id, `users`.`name` AS name, `users`.`image` AS image, `rooms`.`id` AS roomId, `rooms`.`name` AS roomName,`rooms`.`image` AS roomImage FROM `rooms` JOIN `users_rooms` ON `users_rooms`.`room_id` = `rooms`.`id` JOIN `users` ON `users`.`id` = `users_rooms`.`user_id` WHERE `users`.`id` = ? LIMIT ?,?',
    SELECT_HISTORIES: 'SELECT * FROM `histories` WHERE `user_id` = ?',
    SELECT_USER_FOR_HISTORIES: 'SELECT `id`,`name`,`image` FROM `users` WHERE `id` = ?',
    CHECK_PASSWORD: 'SELECT * FROM `users` WHERE `id` = ? AND `password` = SHA1(?)',
    SELECT_USER_BY_ID: 'SELECT `id`,`name`,`image`,`email`,`phone` FROM `users` WHERE `id` = ? AND `status` = 1',
    SELECT_STATUS_FRIEND: 'SELECT `status` FROM `friends` WHERE `user_id` = ? AND `friend_id` = ?',
    REGISTER_USER: 'INSERT IGNORE INTO `users`(`name`, `image`, `password`, `email`, `phone`) VALUES (?,?,SHA1(?),?,?)',
    UPDATE_USER_INFO: 'UPDATE `users` SET `name`= ?,`image`= ?, `update_date`= CURRENT_TIMESTAMP() WHERE `id` = ? AND `update_date` = ?',
    UPDATE_STATUS_USER: 'UPDATE `users` SET `status`= ? WHERE `id` = ?',
    SELECT_USER_ID_BY_EMAIL: 'SELECT `id` FROM `users` WHERE `email` = ?',
    SELECT_UPDATE_DATE_BY_USER_ID: 'SELECT `update_date` as updateDate  FROM `users` WHERE `id` = ?',
    CHANGE_PASSWORD_FORGOT: 'UPDATE `users` SET `password`= SHA1(?),`update_date`= CURRENT_TIMESTAMP() WHERE `update_date` = ? AND `id` = ?',
    CHANGE_PASSWORD: 'UPDATE `users` SET `password`= SHA1(?),`update_date`= CURRENT_TIMESTAMP() WHERE `id` = ? AND `update_date` = ? AND `password` = SHA1(?)',
    INSERT_USER_SEARCH_HISTORY: 'INSERT IGNORE INTO `histories`( `user_id`,  `user_search`) VALUES (?,?)',
    SELECT_LAST_CHAT_CONTENT_IN_ROOM: 'SELECT `chat_contents`.`content`, `users`.`name`, `users`.`image` FROM `chat_contents` JOIN `rooms` ON `chat_contents`.`room_id` = `rooms`.`id` JOIN `users` ON `users`.`id` = `chat_contents`.`user_id` WHERE `rooms`.`id` = ? ORDER BY `chat_contents`.`id` DESC LIMIT 0,1',
}

const transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    auth: {
        user: serverEmail,
        pass: serverPassword
    }
}));

module.exports = class User {
    static query(req, res, sql, values, queryFunction) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            }
            else if (!results || !results.length) {
                return res.status(204).send({});
            }
            else {
                return queryFunction(req, res, results);
            }
        });
    }

    static insertOrUpdate(req, res, sql, values, queryFunction) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            }
            else {
                return queryFunction(req, res, results);
            }
        });
    }

    static insertOrUpdate(req, res, sql, values, queryFunction, data) {
        dbConn.query(sql, values, function (error, results) {
            if (error) {
                return res.status(400).send({});
            }
            else {
                return queryFunction(req, res, results, data);
            }
        });
    }

    static checkUser(req) {
        return !!req.session.user || !!req.cookies.user;
    }

    static searchUser(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }

        if (!req.query || !req.query.search) {
            return res.status(400).send({});
        }

        const search = `%${req.query.search}%`;
        const start = req.query && req.query.page ? per_page * (req.query.page - 1) : 0;
        const values = [search, search, search, start, per_page + 1];
        return User.query(req, res, SQL.SEARCH_USER, values, User.queryUserSearch);
    }

    static queryUserSearch(req, res, results) {
        const isEnd = results.length <= per_page;

        results = Image.genUserImageLink(results);

        if (!isEnd) {
            results.pop();
        }

        const data = {
            isEnd: isEnd,
            usersSearch: results
        }

        const key_search = req.query.search;
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id, key_search];
        return User.insertOrUpdate(req, res, SQL.INSERT_KEY_SEARCH_HISTORY, values, User.removeSearch, data);
    }

    static login(req, res) {
        if (!req.body.email
            || !req.body.password
            || !regexPassword.test(req.body.password)
            || !regexEmail.test(req.body.email)) {
            return res.status(400).send({});
        }

        const email = req.body.email.toLowerCase();

        const password = req.body.password;

        const values = [email, password];
        return User.query(req, res, SQL.SELECT_USER_LOGIN, values, User.queryLogin);
    }

    static queryLogin(req, res, results) {
        results = Image.genUserImageLink(results);
        res.cookie('user', results[0], { secure: true, sameSite: 'none' });
        req.session.user = results[0];
        req.session.cookie.user = results[0];
        return res.status(200).send(results[0]);
    }

    static getProfile(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id];
        return User.query(req, res, SQL.SELECT_PROFILE_BY_ID, values, User.queryGetProfile);
    }

    static queryGetProfile(req, res, results) {
        results = Image.genUserImageLink(results);
        return res.status(200).send(results[0]);
    }

    static getRooms(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const start = req.query && req.query.page ? per_page * (req.query.page - 1) : 0;
        const values = [id, start, per_page + 1];
        return User.query(req, res, SQL.SELECT_ROOM_BY_USER_ID, values, User.queryGetRooms);
    }

    static queryGetRooms(req, res, results) {
        const isEnd = results.length <= per_page;
        results = Image.genUserImageLink(results);
        if (!isEnd) {
            results.pop();
        }
        let tempData = { ...results[0], rooms: [], isEnd: isEnd };
        delete tempData.roomId;
        delete tempData.roomName;
        delete tempData.roomImage;

        let queryLastChatContent = [];
        results.forEach((item) => {
            queryLastChatContent.push(new Promise((resolve, reject) => {
                dbConn.query(SQL.SELECT_LAST_CHAT_CONTENT_IN_ROOM, [item.roomId], function (error, results) {
                    if(results[0]){
                        results[0].image = Image.genImageLink(results[0].image);
                    }
                    tempData.rooms.push({ id: item.roomId, name: item.roomName, image: Image.genImageLink(item.roomImage), lastContent: results[0] })
                    resolve();
                });
            }));
        });

        Promise.all([...queryLastChatContent]).then(() => {
            return res.status(200).send(tempData);
        })
    }

    static getHistories(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id];
        return User.query(req, res, SQL.SELECT_HISTORIES, values, User.queryGetHistories)
    }

    static queryGetHistories(req, res, results) {
        let query_user = [];
        let data = [];
        results.forEach((result) => {
            if (result.user_search) {
                query_user.push(new Promise((resolve) => {
                    dbConn.query(SQL.SELECT_USER_FOR_HISTORIES, [result.user_search], function (error, results) {
                        Image.genUserImageLink(results);
                        data.push(results[0]);
                        resolve();
                    })
                }));
            } else {
                data.push({ key_search: result.key_search });
            }
        });
        Promise.all(query_user).then(() => {
            return res.status(200).send(data);
        })
    }

    static checkPassword(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.password
            || !regexPassword.test(req.body.password)) {
            return res.status(400).send({});
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const password = req.body.password;
        const values = [id, password];
        return User.query(req, res, SQL.CHECK_PASSWORD, values, (req, res, results) => res.status(200).send({}));
    }

    static getInfoById(req, res) {
        if (!req.params.id) {
            return res.status(404).send({});
        }
        const id = req.params.id;
        const values = [id];
        return User.query(req, res, SQL.SELECT_USER_BY_ID, values, User.queryInfoById);
    }

    static queryInfoById(req, res, results) {
        let selectStatusFriend = new Promise((resolve) => { resolve(); });
        results[0].status = -1;
        if (req.session.user || req.cookies.user) {
            const id = req.params.id;
            const userId = req.session.user ? req.session.user.id : req.cookies.user.id;
            selectStatusFriend = new Promise((resolve) => {
                dbConn.query(SQL.SELECT_STATUS_FRIEND, [userId, id], function (error, resultsStatus) {
                    if (resultsStatus && resultsStatus.length) {
                        results[0].status = resultsStatus[0].status;
                    }
                    resolve();
                })
            });

        }
        selectStatusFriend.then(() => {
            results = Image.genUserImageLink(results);
            return res.status(200).send(results[0]);
        })
    }

    static register(req, res) {
        if (!req.body.name
            || !req.body.password
            || !req.body.email
            || !req.body.phone
            || !regexPassword.test(req.body.password)
            || !regexEmail.test(req.body.email)
            || !regexPhone.test(req.body.phone)) {
            return res.status(400).send({});
        }
        const name = req.body.name;
        const password = req.body.password;
        const email = req.body.email.toLowerCase();
        const phone = req.body.phone;
        let image = defaultUserAvatar;
        if (req.file) {
            const imagePath = process.env.IMG_DIR;
            const fileUpload = new Image(imagePath);
            image = `${Math.floor(Math.random() * 99)}${Number(Date.now())}.png`;
            fileUpload.save(req.file.buffer, image);
        }
        const values = [name, image, password, email, phone];
        return User.insertOrUpdate(req, res, SQL.REGISTER_USER, values, User.queryRegister);
    }

    static queryRegister(req, res, results) {
        const genCodePin = User.genCodePin(req);
        const codePinTemp = genCodePin.codePinTemp;
        const mailOptions = genCodePin.mailOptions;

        transporter.sendMail(mailOptions, function (error) {
            if (error) {
                return res.status(400).send();
            } else {
                const otpRegister = {
                    otp: codePinTemp,
                    time: Date.now() + 60000,
                    userId: results.insertId
                }
                res.cookie('otpRegister', otpRegister, { secure: true, sameSite: 'none', expires: new Date(Date.now() + 600000) });
                req.session.otpRegister = otpRegister;
                req.session.cookie.otpRegister = otpRegister;
                return res.status(201).send({});
            }
        });
    }

    static changeInfo(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.name || !req.body.updateDate || !req.body.oldImage) {
            return res.status(400).send({});
        }

        const name = req.body.name;
        const updateDate = new Date(req.body.updateDate);
        let image = Image.genImageNameFromLink(req.body.oldImage);
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const imagePath = process.env.IMG_DIR;
        const fileUpload = new Image(imagePath);
        if (image != defaultUserAvatar && req.file) {
            Image.remove(image);
        }
        if (req.file) {
            image = `${Math.floor(Math.random() * 99)}${Number(Date.now())}.png`;
            fileUpload.save(req.file.buffer, image);
        }
        const values = [name, image, id, updateDate];

        return User.insertOrUpdate(req, res, SQL.UPDATE_USER_INFO, values, User.queryChangeInfo);
    }

    static queryChangeInfo(req, res, results) {
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id];
        return User.query(req, res, SQL.SELECT_PROFILE_BY_ID, values, User.queryLogin);
    }

    static otpRegister(req, res) {
        if (!req.body.otp || !regexOtp.test(req.body.otp) || !req.session.otpRegister || !req.cookies.otpRegister) {
            return res.status(400).send({});
        }
        if (req.session.otpRegister.otp != req.cookies.otpRegister.otp || req.session.otpRegister.time != req.cookies.otpRegister.time || req.session.otpRegister.userId != req.cookies.otpRegister.userId || req.session.otpRegister.time < Date.now()) {
            return res.status(400).send({});
        }
        if (req.body.otp != req.session.otpRegister.otp) {
            return res.status(400).send({});
        }
        const id = req.session.otpRegister.userId;
        const status = 1;
        const values = [status, id];
        return User.insertOrUpdate(req, res, SQL.UPDATE_STATUS_USER, values, User.queryOtpRegister);
    }

    static queryOtpRegister(req, res, results) {
        res.clearCookie('otpRegister');
        delete req.session.otpRegister;
        return res.status(200).send({});
    }

    static forgotPassword(req, res) {
        if (!req.body.email || !regexEmail.test(req.body.email)) {
            res.status(404).send({});
        }
        const email = req.body.email.toLowerCase();
        const values = [email];
        return User.query(req, res, SQL.SELECT_USER_ID_BY_EMAIL, values, User.queryForgotPassword);
    }

    static queryForgotPassword(req, res, results) {
        const genCodePin = User.genCodePin(req);
        const codePinTemp = genCodePin.codePinTemp;
        const mailOptions = genCodePin.mailOptions;

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                return res.status(400).send();
            } else {
                const otpForgot = {
                    otp: codePinTemp,
                    time: Date.now() + 60000,
                    userId: results[0].id,
                }
                res.cookie('otpForgot', otpForgot, { secure: true, sameSite: 'none', expires: new Date(Date.now() + 600000) });
                req.session.otpForgot = otpForgot;
                req.session.cookie.otpForgot = otpForgot;
                return res.status(200).send({});
            }
        });
    }

    static genCodePin(req) {
        const codePinTemp = Math.random().toString().substr(2, 6);
        const email = req.body.email.toLowerCase();
        const mailOptions = {
            from: serverEmail,
            to: email,
            subject: 'OTP :' + codePinTemp,
            text: 'Mã code chỉ có hiệu lực trong 1 phút \n OTP: ' + codePinTemp
        };
        return {
            mailOptions: mailOptions, codePinTemp: codePinTemp
        }
    }

    static otpForgotPassword(req, res) {
        if (!req.body.otp || !regexOtp.test(req.body.otp) || !req.session.otpForgot || !req.cookies.otpForgot) {
            return res.status(400).send({});
        }
        if (req.session.otpForgot.otp != req.cookies.otpForgot.otp || req.session.otpForgot.time != req.cookies.otpForgot.time || req.session.otpForgot.userId != req.cookies.otpForgot.userId || req.session.otpForgot.time < Date.now()) {
            return res.status(400).send({});
        }
        if (req.body.otp != req.session.otpForgot.otp) {
            return res.status(400).send({});
        }

        const id = req.session.otpForgot.userId;
        const values = [id];
        return User.query(req, res, SQL.SELECT_UPDATE_DATE_BY_USER_ID, values, User.queryOtpForgotPassword);
    }

    static queryOtpForgotPassword(req, res, results) {
        const otp = req.session.otpForgot.otp;
        const id = req.session.otpForgot.userId;
        const otpForgot = {
            otp: otp,
            userId: id,
        }
        res.clearCookie('otpForgot');
        res.cookie('otpForgot', otpForgot, { secure: true, sameSite: 'none' });
        req.session.otpForgot = otpForgot;
        req.session.cookie.otpForgot = otpForgot;
        return res.status(200).send({ updateDate: results[0].updateDate });
    }

    static changeForgotPassword(req, res) {
        if (!req.body.otp
            || !req.body.newPassword
            || !req.body.updateDate
            || !regexOtp.test(req.body.otp)
            || !regexPassword.test(req.body.newPassword)
            || !req.session.otpForgot
            || !req.cookies.otpForgot) {
            return res.status(400).send({});
        }
        if (req.session.otpForgot.otp != req.cookies.otpForgot.otp
            || req.session.otpForgot.userId != req.cookies.otpForgot.userId) {
            return res.status(400).send({});
        }
        if (req.body.otp != req.session.otpForgot.otp) {
            return res.status(400).send({});
        }

        const id = req.session.otpForgot.userId;
        const updateDate = new Date(req.body.updateDate);
        const password = req.body.newPassword;
        const values = [password, updateDate, id];
        return User.insertOrUpdate(req, res, SQL.CHANGE_PASSWORD_FORGOT, values, User.queryChangeForgotPassword);
    }

    static queryChangeForgotPassword(req, res, results) {
        res.clearCookie('otpForgot');
        delete req.session.otpForgot;
        return res.status(200).send({});
    }

    static changePassword(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.newPassword
            || !req.body.oldPassword
            || !req.body.updateDate
            || !regexPassword.test(req.body.newPassword)) {
            return res.status(400).send();
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const updateDate = new Date(req.body.updateDate);
        const password = req.body.newPassword;
        const oldPassword = req.body.oldPassword;
        const values = [password, id, updateDate, oldPassword];
        return User.insertOrUpdate(req, res, SQL.CHANGE_PASSWORD, values, (req, res, results) => res.status(200).send({}));
    }

    static historySearch(req, res) {
        if (!User.checkUser(req, res)) {
            return res.status(403).send({});
        }
        if (!req.body.userId) {
            return res.status(400).send();
        }
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const userId = req.body.userId;
        const values = [id, userId];
        return User.insertOrUpdate(req, res, SQL.INSERT_USER_SEARCH_HISTORY, values, User.removeSearch, {});
    }

    static removeSearch(req, res, results, data) {
        const id = req.session.user ? req.session.user.id : req.cookies.user.id;
        const values = [id];
        return User.insertOrUpdate(req, res, SQL.REMOVE_HISTORY, values, (req, res, results) => res.status(200).send(data));
    }
}
