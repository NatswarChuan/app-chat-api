
const fs = require('fs');
const path = require('path');
const sharp = require('sharp');
const IpAddress = require('../config/IpAddress/index');
const port = process.env.PORT;
module.exports =  class Image {
    constructor(folder) {
        this.folder = folder;
    }
    async save(buffer, fileName) {
        const filename = fileName ;
        const filepath = this.filepath(filename);

        await sharp(buffer).toFile(filepath);

        return filename;
    }

    filepath(filename) {
        return path.resolve(`${this.folder}/${filename}`)
    }

    static remove(filename) {
        fs.unlink(this.folder + filename, (err) => {
        });
    }

    static genUserImageLink(user) {
        const ip = IpAddress.getIpAddress().pop();
        user = user.map((result) => {
            result.image = Image.genImageLink(result.image);
            return result;
        });
        return user;
    }
    
    static genImageLink(image) {
        const ip = IpAddress.getIpAddress().pop();
        return `http://${ip}:${port}/api/image/get/${image}`;
    }

    static genImageNameFromLink(link) {
        const ip = IpAddress.getIpAddress().pop();
        return link.replace(`http://${ip}:${port}/api/image/get/`,'');
    }
    
}