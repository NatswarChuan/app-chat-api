const Friend = require('../models/Friend');

module.exports = class FriendController {

    static getFiends = (req, res) => Friend.getFiends(req, res);

    static addFriend = (req, res) => Friend.addFriend(req, res);

    static acceptedFiends = (req, res) => Friend.acceptedFiends(req, res);
    
    static unFriend = (req, res) => Friend.unFriend(req, res);
}