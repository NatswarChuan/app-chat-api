const Room = require('../models/Room');

module.exports = class RoomController {

    static create = (req, res) => Room.create(req, res);
    
    static getUsersByRoomId = (req,res) => Room.getUsersByRoomId(req, res);

    static inviteUserToRoomId = (req, res) => Room.inviteUserToRoomId(req, res);

    static leavingRoom = (req,res)=> Room.leavingRoom(req, res);

    static updateRoom = (req,res)=> Room.updateRoom(req, res);
}