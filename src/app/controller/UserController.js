const User = require('../models/User');

module.exports = class UserController {

    static searchUser = (req, res) => User.searchUser(req, res);

    static login = (req, res) => User.login(req, res);

    static getProfile = (req, res) => User.getProfile(req, res);

    static logout = (req, res) => {
        let checkStatusError = true;
        if (req.session.user) {
            delete req.session.user;
            checkStatusError = false;
        }
        if (req.cookies.user) {
            res.clearCookie("user");
            checkStatusError = false;
        }
        return res.status(!checkStatusError ? 200 : 400).send({});
    }

    static getRooms = (req, res) => User.getRooms(req, res);

    static getHistories = (req, res) => User.getHistories(req, res);

    static checkPassword = (req, res) => User.checkPassword(req, res);

    static getInfoById = (req, res) => User.getInfoById(req, res);

    static register = (req, res) => User.register(req, res);

    static changeInfo = (req, res) => User.changeInfo(req, res);

    static otpRegister = (req, res) => User.otpRegister(req, res);

    static forgotPassword = (req, res) => User.forgotPassword(req, res);

    static otpForgotPassword = (req, res) => User.otpForgotPassword(req, res);

    static changeForgotPassword = (req, res) => User.changeForgotPassword(req, res);

    static changePassword = (req, res) => User.changePassword(req, res);

    static historySearch= (req, res) => User.historySearch(req, res);
}